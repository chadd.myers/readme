# NSF NOIRLab GitLab Group

This GitLab group, https://gitlab.com/nsf-noirlab, can serve as a source
code management platform for any department at the NOIRLab. Different
departments will have their own subgroup (e.g. the Community Science and
Data Center scopes projects under https://gitlab.com/nsf-noirlab/csdc).

## Getting Access

For now you can click the "Request Access" button at https://gitlab.com/nsf-noirlab
or email Nicholas Wolf (nic.wolf@noirlab.edu) with a request to be invited. Please
note your department/team (Gemini, CSDC, etc.) and I'll grant you the appropriate
permissions.

## Managing Permissions

Users added to the nsf-noirlab gitlab group or any of its subgroups and projects
should be added following proper protocol. When adding a new user, evaluate the
access requirements of the user and set the role accordingly. Consider if the user
should be permitted to push and pull code for each project in a group or if the
user only requires access to a single project. Consider if this group has any
subgroups in which the user does not need certain access to. Permissions in gitlab
are inherited from parent groups so always consider the subgroups and projects
that are present. Details about each role follow:

**Maintainer:**  
The 'maintainer' role should be reserved for persons in charge of a particular
group instance and all of its subgroups OR persons who are managing all projects
within a group including its subgroups. Using 'owner' or 'maintainer' access at the
group level will give the user access to settings for all projects in the group
(including new projects and subgroup's projects) which could lead to accidental errors
or unwanted exposure of secrets (in the form of CI variables).

**Developer:**  
The 'developer' role is considered a safe default when you want to allow a staff
member to access your group's projects. Users with the developer role will be able
to perform most actions in gitlab but will not be able to alter project settings or
push to or delete protected branches. If a non staff member needs developer access
it is best to grant this at the project level.

**Reporter:**  
The 'reporter' role is typically used for project managers or non developers who
are involved in the development process. They will be able to view the repository
and manage some aspects of the project such as managing issues, viewing registry
information and viewing pipeline information.

**Guest:**  
The 'guest' role is typically used to grant view access to a particular group or
project without giving them the ability to create or alter data within the
group or project. Keep in mind if there is sensitive information within the repository
files guests will be able to view these.

Full details about gitlabs permissions can be found here:
https://docs.gitlab.com/ee/user/permissions.html

## Jira Integration

The free version of GitLab includes a basic Jira integration.

The free features are:
- merge requests (MRs) and commits automatically link to Jira tickets
- Jira tickets automatically link to MRs and commits
- Optional: merging a MR automatically marks a ticket as Done

This is very easy to set up and is confirmed to work with Jira Cloud and Jira versions 6.x and up.

docs: https://docs.gitlab.com/ee/user/project/integrations/jira.html#view-jira-issues

Contact Carl for more info.

## Support

Nicholas Wolf (nic.wolf@noirlab.edu)

Carl Stubens (carl.stubens@noirlab.edu)
